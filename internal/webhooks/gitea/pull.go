package gitea

import (
	"encoding/json"
	"fmt"
	"log"

	giteaSdk "codeberg.org/justusbunsi/gitea-sonarqube-bot/internal/clients/gitea"
	sqSdk "codeberg.org/justusbunsi/gitea-sonarqube-bot/internal/clients/sonarqube"
	"codeberg.org/justusbunsi/gitea-sonarqube-bot/internal/settings"
)

type pullRequest struct {
	Number int64 `json:"number"`
	Head   struct {
		Sha string `json:"sha"`
	} `json:"head"`
}

type repoOwner struct {
	Login string `json:"login"`
}

type rawRepository struct {
	Name  string    `json:"name"`
	Owner repoOwner `json:"owner"`
}

type PullWebhook struct {
	Action             string        `json:"action"`
	PullRequest        pullRequest   `json:"pull_request"`
	RawRepository      rawRepository `json:"repository"`
	Repository         settings.GiteaRepository
	ConfiguredProjects []settings.Project
}

func (w *PullWebhook) loadConfiguredProjects(p []settings.Project) {
	owner := w.RawRepository.Owner.Login
	name := w.RawRepository.Name
	for _, proj := range p {
		if proj.Gitea.Owner == owner && proj.Gitea.Name == name {
			w.ConfiguredProjects = append(w.ConfiguredProjects, proj)
		}
	}
}

func (w *PullWebhook) Validate() error {
	w.loadConfiguredProjects(settings.Projects)
	if len(w.ConfiguredProjects) == 0 {
		return fmt.Errorf("ignore hook for non-configured project '%s/%s'", w.RawRepository.Owner.Login, w.RawRepository.Name)
	}

	if w.Action != "synchronized" && w.Action != "opened" {
		return fmt.Errorf("ignore hook for action others than 'opened' or 'synchronized'")
	}

	w.Repository = settings.GiteaRepository{
		Owner: w.RawRepository.Owner.Login,
		Name:  w.RawRepository.Name,
	}

	return nil
}

func (w *PullWebhook) ProcessData(gSDK giteaSdk.GiteaSdkInterface, sqSDK sqSdk.SonarQubeSdkInterface) {
	for _, configuredProject := range w.ConfiguredProjects {
		_ = gSDK.UpdateStatus(configuredProject, w.PullRequest.Head.Sha, giteaSdk.StatusDetails{
			Url:     "",
			Message: "Analysis pending...",
			State:   giteaSdk.StatusPending,
		})
	}
}

func NewPullWebhook(raw []byte) (*PullWebhook, bool) {
	w := &PullWebhook{}
	err := json.Unmarshal(raw, &w)
	if err != nil {
		log.Printf("Error parsing Gitea webhook: %s", err.Error())
		return w, false
	}

	return w, true
}
