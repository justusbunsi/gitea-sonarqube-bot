package actions

import (
	"codeberg.org/justusbunsi/gitea-sonarqube-bot/internal/settings"
	"fmt"
	"strings"
)

type BotAction string

const (
	ActionReview BotAction = "/sq-bot review"
	ActionPrefix string    = "/sq-bot"
)

func IsValidBotComment(c string, projects []settings.Project) (bool, *settings.Project) {
	if !strings.HasPrefix(c, ActionPrefix) {
		return false, nil
	}

	for _, project := range projects {
		if strings.Compare(c, fmt.Sprintf("%s %s", ActionReview, project.SonarQube.Key)) == 0 {
			return true, &project
		}
	}

	return strings.Compare(c, string(ActionReview)) == 0, nil
}
