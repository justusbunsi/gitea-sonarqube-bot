package actions

import (
	"codeberg.org/justusbunsi/gitea-sonarqube-bot/internal/settings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsValidBotComment(t *testing.T) {
	configuredProjects := []settings.Project{
		{
			SonarQube: struct {
				Key string
			}{Key: "test-project"},
		},
	}

	t.Run("Valid", func(t *testing.T) {
		validationResult, specificProject := IsValidBotComment("/sq-bot review", configuredProjects)
		assert.True(t, validationResult, "[validationResult] Correct bot comment not recognized")
		assert.Nil(t, specificProject, "[specificProject] Correct bot comment not recognized")

		validationResult, specificProject = IsValidBotComment("/sq-bot review test-project", configuredProjects)
		assert.True(t, validationResult, "[validationResult] Correct project-specific bot comment not recognized")
		assert.Equal(t, configuredProjects[0], *specificProject, "[specificProject] Correct project-specific bot comment not recognized")

	})

	t.Run("Invalid", func(t *testing.T) {
		validationResult, specificProject := IsValidBotComment("", configuredProjects)
		assert.False(t, validationResult, "[validationResult] Undetected missing action prefix")
		assert.Nil(t, specificProject, "[specificProject] Undetected missing action prefix")

		validationResult, specificProject = IsValidBotComment("/sq-bot invalid-command", configuredProjects)
		assert.False(t, validationResult, "[validationResult] Undetected invalid bot command")
		assert.Nil(t, specificProject, "[specificProject] Undetected invalid bot command")

		validationResult, specificProject = IsValidBotComment("/sq-bot review non-configured-project", configuredProjects)
		assert.False(t, validationResult, "[validationResult] Undetected non-configured project bot command")
		assert.Nil(t, specificProject, "[specificProject] Undetected non-configured project bot command")

		validationResult, specificProject = IsValidBotComment("/sq-bot review test-project should not be detected", configuredProjects)
		assert.False(t, validationResult, "[validationResult] Undetected project-specific invalid bot command")
		assert.Nil(t, specificProject, "[specificProject] Undetected project-specific invalid bot command")

		validationResult, specificProject = IsValidBotComment("Some context with /sq-bot review within", configuredProjects)
		assert.False(t, validationResult, "[validationResult] Incorrect bot prefix detected inside random comment")
		assert.Nil(t, specificProject, "[specificProject] Incorrect bot prefix detected inside random comment")

	})
}
