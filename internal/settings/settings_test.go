package settings

import (
	"os"
	"path"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func defaultConfig() []byte {
	return []byte(
		`gitea:
  url: https://example.com/gitea
  token:
    value: d0fcdeb5eaa99c506831f9eb4e63fc7cc484a565
  webhook:
    secret: haxxor-gitea-secret
  statusCheck:
    name: "gitea-sonarqube-bot"
sonarqube:
  url: https://example.com/sonarqube
  token:
    value: a09eb5785b25bb2cbacf48808a677a0709f02d8e
  webhook:
    secret: haxxor-sonarqube-secret
  additionalMetrics: []
projects:
  - sonarqube:
      key: gitea-sonarqube-bot
    gitea:
      owner: example-organization
      name: pr-bot
namingPattern:
  regex: "^PR-(\\d+)$"
  template: "PR-%d"
`)
}

func WriteConfigFile(t *testing.T, content []byte) string {
	dir := os.TempDir()
	config := path.Join(dir, "config.yaml")

	t.Cleanup(func() {
		_ = os.Remove(config)
	})

	_ = os.WriteFile(config, content, 0444)

	return config
}

func TestLoad(t *testing.T) {
	t.Run("Missing file", func(t *testing.T) {
		assert.Panics(t, func() { Load(path.Join(os.TempDir(), "config.yaml")) }, "No panic while reading missing file")
	})

	t.Run("Existing file", func(t *testing.T) {
		c := WriteConfigFile(t, defaultConfig())
		assert.NotPanics(t, func() { Load(c) }, "Unexpected panic while reading existing file")
	})

	t.Run("File references", func(t *testing.T) {
		giteaWebhookSecretFile := path.Join(os.TempDir(), "webhook-secret-gitea")
		_ = os.WriteFile(giteaWebhookSecretFile, []byte(`gitea-totally-secret`), 0444)

		giteaTokenFile := path.Join(os.TempDir(), "token-secret-gitea")
		_ = os.WriteFile(giteaTokenFile, []byte(`d0fcdeb5eaa99c506831f9eb4e63fc7cc484a565`), 0444)

		sonarqubeWebhookSecretFile := path.Join(os.TempDir(), "webhook-secret-sonarqube")
		_ = os.WriteFile(sonarqubeWebhookSecretFile, []byte(`sonarqube-totally-secret`), 0444)

		sonarqubeTokenFile := path.Join(os.TempDir(), "token-secret-sonarqube")
		_ = os.WriteFile(sonarqubeTokenFile, []byte(`a09eb5785b25bb2cbacf48808a677a0709f02d8e`), 0444)

		c := WriteConfigFile(t, []byte(
			`gitea:
  url: https://example.com/gitea
  token:
    value: fake-gitea-token
sonarqube:
  url: https://example.com/sonarqube
  token:
    value: fake-sonarqube-token
projects:
  - sonarqube:
      key: gitea-sonarqube-bot
    gitea:
      owner: example-organization
      name: pr-bot
`))
		_ = os.Setenv("PRBOT_GITEA_WEBHOOK_SECRETFILE", giteaWebhookSecretFile)
		_ = os.Setenv("PRBOT_GITEA_TOKEN_FILE", giteaTokenFile)
		_ = os.Setenv("PRBOT_SONARQUBE_WEBHOOK_SECRETFILE", sonarqubeWebhookSecretFile)
		_ = os.Setenv("PRBOT_SONARQUBE_TOKEN_FILE", sonarqubeTokenFile)

		expectedGitea := GiteaConfig{
			Url: "https://example.com/gitea",
			Token: &Token{
				Value: "d0fcdeb5eaa99c506831f9eb4e63fc7cc484a565",
				file:  giteaTokenFile,
			},
			Webhook: &Webhook{
				Secret:     "gitea-totally-secret",
				secretFile: giteaWebhookSecretFile,
			},
			StatusCheck: &StatusCheck{
				Name: "gitea-sonarqube-bot",
			},
		}

		expectedSonarQube := SonarQubeConfig{
			Url: "https://example.com/sonarqube",
			Token: &Token{
				Value: "a09eb5785b25bb2cbacf48808a677a0709f02d8e",
				file:  sonarqubeTokenFile,
			},
			Webhook: &Webhook{
				Secret:     "sonarqube-totally-secret",
				secretFile: sonarqubeWebhookSecretFile,
			},
			AdditionalMetrics: []string{},
		}

		Load(c)
		assert.EqualValues(t, expectedGitea, Gitea)
		assert.EqualValues(t, expectedSonarQube, SonarQube)

		t.Cleanup(func() {
			_ = os.Remove(giteaWebhookSecretFile)
			_ = os.Remove(giteaTokenFile)
			_ = os.Remove(sonarqubeWebhookSecretFile)
			_ = os.Remove(sonarqubeTokenFile)
			_ = os.Unsetenv("PRBOT_GITEA_WEBHOOK_SECRETFILE")
			_ = os.Unsetenv("PRBOT_GITEA_TOKEN_FILE")
			_ = os.Unsetenv("PRBOT_SONARQUBE_WEBHOOK_SECRETFILE")
			_ = os.Unsetenv("PRBOT_SONARQUBE_TOKEN_FILE")
		})
	})
}

func TestLoadGitea(t *testing.T) {
	t.Run("Default", func(t *testing.T) {
		c := WriteConfigFile(t, defaultConfig())
		Load(c)

		expected := GiteaConfig{
			Url: "https://example.com/gitea",
			Token: &Token{
				Value: "d0fcdeb5eaa99c506831f9eb4e63fc7cc484a565",
			},
			Webhook: &Webhook{
				Secret: "haxxor-gitea-secret",
			},
			StatusCheck: &StatusCheck{
				Name: "gitea-sonarqube-bot",
			},
		}

		assert.EqualValues(t, expected, Gitea)
	})

	t.Run("Internal defaults", func(t *testing.T) {
		c := WriteConfigFile(t, []byte(
			`gitea:
  url: https://example.com/gitea
  token:
    value: fake-gitea-token
sonarqube:
  url: https://example.com/sonarqube
  token:
    value: fake-sonarqube-token
  additionalMetrics: "new_security_hotspots"
projects:
  - sonarqube:
      key: gitea-sonarqube-bot
    gitea:
      owner: example-organization
      name: pr-bot
`))
		Load(c)

		expected := GiteaConfig{
			Url: "https://example.com/gitea",
			Token: &Token{
				Value: "fake-gitea-token",
			},
			Webhook: &Webhook{
				Secret: "",
			},
			StatusCheck: &StatusCheck{
				Name: "gitea-sonarqube-bot",
			},
		}

		assert.EqualValues(t, expected, Gitea)
	})

	t.Run("Injected envs", func(t *testing.T) {
		_ = os.Setenv("PRBOT_GITEA_WEBHOOK_SECRET", "injected-webhook-secret")
		_ = os.Setenv("PRBOT_GITEA_TOKEN_VALUE", "injected-token")
		_ = os.Setenv("PRBOT_GITEA_STATUSCHECK_NAME", "test-status-check-name")
		c := WriteConfigFile(t, defaultConfig())
		Load(c)

		expected := GiteaConfig{
			Url: "https://example.com/gitea",
			Token: &Token{
				Value: "injected-token",
			},
			Webhook: &Webhook{
				Secret: "injected-webhook-secret",
			},
			StatusCheck: &StatusCheck{
				Name: "test-status-check-name",
			},
		}

		assert.EqualValues(t, expected, Gitea)

		t.Cleanup(func() {
			_ = os.Unsetenv("PRBOT_GITEA_WEBHOOK_SECRET")
			_ = os.Unsetenv("PRBOT_GITEA_TOKEN_VALUE")
			_ = os.Unsetenv("PRBOT_GITEA_STATUSCHECK_NAME")
		})
	})
}

func TestLoadSonarQube(t *testing.T) {
	t.Run("Default", func(t *testing.T) {
		c := WriteConfigFile(t, defaultConfig())
		Load(c)

		expected := SonarQubeConfig{
			Url: "https://example.com/sonarqube",
			Token: &Token{
				Value: "a09eb5785b25bb2cbacf48808a677a0709f02d8e",
			},
			Webhook: &Webhook{
				Secret: "haxxor-sonarqube-secret",
			},
		}

		assert.EqualValues(t, expected, SonarQube)
		assert.EqualValues(t, expected.GetMetricsList(), "bugs,vulnerabilities,code_smells")
	})

	t.Run("Additional metrics", func(t *testing.T) {
		c := WriteConfigFile(t, []byte(
			`gitea:
  url: https://example.com/gitea
  token:
    value: fake-gitea-token
sonarqube:
  url: https://example.com/sonarqube
  token:
    value: fake-sonarqube-token
  additionalMetrics: "new_security_hotspots"
projects:
  - sonarqube:
      key: gitea-sonarqube-bot
    gitea:
      owner: example-organization
      name: pr-bot
`))
		Load(c)

		expected := SonarQubeConfig{
			Url: "https://example.com/sonarqube",
			Token: &Token{
				Value: "fake-sonarqube-token",
			},
			Webhook: &Webhook{
				Secret: "",
			},
			AdditionalMetrics: []string{
				"new_security_hotspots",
			},
		}

		assert.EqualValues(t, expected, SonarQube)
		assert.EqualValues(t, expected.AdditionalMetrics, []string{"new_security_hotspots"})
		assert.EqualValues(t, "bugs,vulnerabilities,code_smells,new_security_hotspots", SonarQube.GetMetricsList())
	})

	t.Run("Injected envs", func(t *testing.T) {
		_ = os.Setenv("PRBOT_SONARQUBE_WEBHOOK_SECRET", "injected-webhook-secret")
		_ = os.Setenv("PRBOT_SONARQUBE_TOKEN_VALUE", "injected-token")
		c := WriteConfigFile(t, defaultConfig())
		Load(c)

		expected := SonarQubeConfig{
			Url: "https://example.com/sonarqube",
			Token: &Token{
				Value: "injected-token",
			},
			Webhook: &Webhook{
				Secret: "injected-webhook-secret",
			},
		}

		assert.EqualValues(t, expected, SonarQube)

		t.Cleanup(func() {
			_ = os.Unsetenv("PRBOT_SONARQUBE_WEBHOOK_SECRET")
			_ = os.Unsetenv("PRBOT_SONARQUBE_TOKEN_VALUE")
		})
	})
}

func TestLoadProjects(t *testing.T) {
	t.Run("Default", func(t *testing.T) {
		c := WriteConfigFile(t, defaultConfig())
		Load(c)

		expectedProjects := []Project{
			{
				SonarQube: struct{ Key string }{
					Key: "gitea-sonarqube-bot",
				},
				Gitea: GiteaRepository{
					Owner: "example-organization",
					Name:  "pr-bot",
				},
			},
		}

		assert.EqualValues(t, expectedProjects, Projects)
	})

	t.Run("Empty mapping", func(t *testing.T) {
		invalidConfig := []byte(
			`gitea:
  url: https://example.com/gitea
  token:
    value: d0fcdeb5eaa99c506831f9eb4e63fc7cc484a565
  webhook:
    secret: haxxor-gitea-secret
sonarqube:
  url: https://example.com/sonarqube
  token:
    value: a09eb5785b25bb2cbacf48808a677a0709f02d8e
  webhook:
    secret: haxxor-sonarqube-secret
projects: []
`)
		c := WriteConfigFile(t, invalidConfig)

		assert.Panics(t, func() { Load(c) }, "No panic for empty project mapping that is required")
	})

	t.Run("Status check override", func(t *testing.T) {
		config := []byte(`
projects:
  - sonarqube:
      key: project-1
    gitea:
      owner: justusbunsi
      name: example-repo
    statusCheck:
      name: "override global status check for this project"
`)
		c := WriteConfigFile(t, config)
		Load(c)

		expectedProjects := []Project{
			{
				SonarQube: struct{ Key string }{
					Key: "project-1",
				},
				Gitea: GiteaRepository{
					Owner: "justusbunsi",
					Name:  "example-repo",
				},
				StatusCheck: &StatusCheck{
					Name: "override global status check for this project",
				},
			},
		}

		assert.EqualValues(t, expectedProjects, Projects)
	})

	t.Run("Partially defined project status check", func(t *testing.T) {
		config := []byte(`
projects:
  - sonarqube:
      key: project-1
    gitea:
      owner: justusbunsi
      name: example-repo
    statusCheck: {}
`)
		c := WriteConfigFile(t, config)
		Load(c)

		expectedProjects := []Project{
			{
				SonarQube: struct{ Key string }{
					Key: "project-1",
				},
				Gitea: GiteaRepository{
					Owner: "justusbunsi",
					Name:  "example-repo",
				},
				StatusCheck: &StatusCheck{
					Name: "",
				},
			},
		}

		assert.EqualValues(t, expectedProjects, Projects)
	})
}

func TestLoadNamingPattern(t *testing.T) {
	t.Run("Default", func(t *testing.T) {
		c := WriteConfigFile(t, defaultConfig())
		Load(c)

		expected := &PatternConfig{
			RegExp:   regexp.MustCompile(`^PR-(\d+)$`),
			Template: "PR-%d",
		}

		assert.EqualValues(t, expected, Pattern)
	})

	t.Run("Internal defaults", func(t *testing.T) {
		c := WriteConfigFile(t, []byte(
			`gitea:
  url: https://example.com/gitea
  token:
    value: fake-gitea-token
sonarqube:
  url: https://example.com/sonarqube
  token:
    value: fake-sonarqube-token
  additionalMetrics: "new_security_hotspots"
projects:
  - sonarqube:
      key: gitea-sonarqube-bot
    gitea:
      owner: example-organization
      name: pr-bot
`))
		Load(c)

		expected := &PatternConfig{
			RegExp:   regexp.MustCompile(`^PR-(\d+)$`),
			Template: "PR-%d",
		}

		assert.EqualValues(t, expected, Pattern)
	})

	t.Run("Injected envs", func(t *testing.T) {
		_ = os.Setenv("PRBOT_NAMINGPATTERN_REGEX", "test-(\\d+)-pullrequest")
		_ = os.Setenv("PRBOT_NAMINGPATTERN_TEMPLATE", "test-%d-pullrequest")
		c := WriteConfigFile(t, defaultConfig())
		Load(c)

		expected := &PatternConfig{
			RegExp:   regexp.MustCompile(`test-(\d+)-pullrequest`),
			Template: "test-%d-pullrequest",
		}

		assert.EqualValues(t, expected, Pattern)

		t.Cleanup(func() {
			_ = os.Unsetenv("PRBOT_NAMINGPATTERN_REGEX")
			_ = os.Unsetenv("PRBOT_NAMINGPATTERN_TEMPLATE")
		})
	})

	t.Run("Mixed input", func(t *testing.T) {
		_ = os.Setenv("PRBOT_NAMINGPATTERN_REGEX", "test-(\\d+)-pullrequest")
		c := WriteConfigFile(t, defaultConfig())
		Load(c)

		expected := &PatternConfig{
			RegExp:   regexp.MustCompile(`test-(\d+)-pullrequest`),
			Template: "PR-%d",
		}

		assert.EqualValues(t, expected, Pattern)

		t.Cleanup(func() {
			_ = os.Unsetenv("PRBOT_NAMINGPATTERN_REGEX")
		})
	})
}
