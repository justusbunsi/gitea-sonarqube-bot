package gitea

import (
	"io"
	"log"
	"os"
	"testing"
)

// SETUP: mute logs
func TestMain(m *testing.M) {
	log.SetOutput(io.Discard)
	os.Exit(m.Run())
}
