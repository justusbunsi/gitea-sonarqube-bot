# Changelog

## v0.4.0

### ☸️ Helm Chart

- Support custom status check name per project
- Bump default image tag to `v0.4.0`

### 🤖 Application

- Support custom status check name per project
- Add SonarQube project name to PR comment
- Support `/sq-bot review` to handle multiple SonarQube projects
- Add `/sq-bot review <project-key>` PR comment action

### 👻 Maintenance

- Update base Docker images
- Require Golang 1.23 for builds
- Update dependencies to newest versions

## v0.3.5

### ☸️ Helm Chart

- Bump default image tag to `v0.3.5`

### 🤖 Application

- Add output for debugging/setting up

### 👻 Maintenance

- Update base Docker images
- Require Golang 1.22 for builds
- Update dependencies to newest versions

## v0.3.4

### ☸️ Helm Chart

- Bump default image tag to `v0.3.4`

### 👻 Maintenance

- Update base Docker images
- Update dependencies to newest versions

## v0.3.3

### ☸️ Helm Chart

- Bump default image tag to `v0.3.3`

### 👻 Maintenance

- Update base Docker images
- Update dependencies to newest versions

## v0.3.2

### ☸️ Helm Chart

- Bump default image tag to `v0.3.2`

### 👻 Maintenance

- Update base Docker images
- Require Golang 1.21 for builds
- Update dependencies to newest versions

## v0.3.1

### ☸️ Helm Chart

- Bump default image tag to `v0.3.1`

### 👻 Maintenance

- Update base Docker images
- Update dependencies to newest versions

## v0.3.0

### 🤖 Application

- Allow PR status check name customization

### ☸️ Helm Chart

- Add `.Values.app.configuration.gitea.statusCheck` parameters
- Bump default image tag to `v0.3.0`

### 👻 Maintenance

- Update base Docker images
- Update dependencies to newest versions

## v0.2.4

### ☸️ Helm Chart

- Bump default image tag to `v0.2.4`

### 🐳 Docker image

- Ensure using latest available apk packages

## v0.2.3

### ☸️ Helm Chart

- Bump default image tag to `v0.2.3`

### 👻 Maintenance

- Update base Docker images
- Update dependencies to newest versions
- Require Golang 1.20 for builds

## v0.2.2

### ☸️ Helm Chart

- Bump default image tag to `v0.2.2`

### 👻 Maintenance

- Remove `fvbock/endless` dependency
- Require Golang 1.19 for builds
- Update base Docker images
- Update dependencies to newest versions

## v0.2.1

### 🤖 Application

- Allow configuring listening port
- Allow changing naming pattern for Pull Requests
- Improve error handling for SonarQube communication

### 🐳 Docker image

- Add `GITEA_SQ_BOT_PORT` environment variable

### ☸️ Helm Chart

- Add `.Values.app.listeningPort` parameter
- Add `.Values.app.configuration.namingPattern` parameters

## v0.2.0

### 🤖 Application

- Add webhook secret validation
- Improve configuration file flexibility
- Stop log output for `/ping` and `/favicon.ico` endpoints

### 🐳 Docker image

- Add `GITEA_SQ_BOT_CONFIG_PATH` environment variable

### ☸️ Helm Chart

- Add `.Values.app.configLocationOverride` parameter
- Bump default image tag to `v0.2.0`

## v0.1.1

### ☸️ Helm Chart

- Bump default image tag to `v0.1.1`

### 👻 Maintenance

- Bump Golang version to 1.18
- Update dependencies to newest versions

## v0.1.0

Initial release
