###################################
# Build stages
###################################
FROM golang:1.23-alpine@sha256:c694a4d291a13a9f9d94933395673494fc2cc9d4777b85df3a7e70b3492d3574 AS build-go

ARG GOPROXY
ENV GOPROXY=${GOPROXY:-direct}

RUN apk update --no-cache \
    && apk upgrade --no-cache \
    && apk --no-cache add bash build-base git

COPY . ${GOPATH}/src/bot
WORKDIR ${GOPATH}/src/bot

RUN go build ./cmd/gitea-sonarqube-bot

###################################
# Production image
###################################
FROM alpine:3.20@sha256:1e42bbe2508154c9126d48c2b8a75420c3544343bf86fd041fb7527e017a4b4a
LABEL maintainer="justusbunsi <sk.bunsenbrenner@gmail.com>"
LABEL org.opencontainers.image.source="https://codeberg.org/justusbunsi/gitea-sonarqube-bot"

RUN apk update --no-cache \
    && apk upgrade --no-cache \
    && apk --no-cache add bash ca-certificates \
    && rm -rf /var/cache/apk/* \
    && addgroup -S -g 1000 bot \
    && adduser -S -D -H -h /home/bot -s /bin/bash -u 1000 -G bot bot \
    && mkdir -p /home/bot/config/ \
    && chown bot:bot /home/bot/config/

COPY --chown=bot:bot --chmod=755 docker /
COPY --from=build-go --chown=bot:bot --chmod=755 /go/src/bot/gitea-sonarqube-bot /usr/local/bin/gitea-sonarqube-bot

#bot:bot
USER 1000:1000
WORKDIR /home/bot
ENV HOME=/home/bot

ENV GIN_MODE="release"
ENV GITEA_SQ_BOT_CONFIG_PATH="/home/bot/config/config.yaml"
ENV GITEA_SQ_BOT_PORT="3000"

EXPOSE $GITEA_SQ_BOT_PORT
VOLUME ["/home/bot/config/"]
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD []
